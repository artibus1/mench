#########################################################################################################################
# This ZSH configuration file was written by the developers of the Anarchy Installer: https://anarchyinstaller.gitlab.io 
# I took it after using their installer to install Arch Linux. I made many modifications to it; mostly, the  aliases 
# added in the section called: my alias, beginning on line 208
#########################################################################################################################



### Set/unset ZSH options
#########################
# setopt NOHUP
# setopt NOTIFY
# setopt NO_FLOW_CONTROL
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt APPEND_HISTORY
# setopt AUTO_LIST
# setopt AUTO_REMOVE_SLASH
# setopt AUTO_RESUME
unsetopt BG_NICE
setopt CORRECT
setopt EXTENDED_HISTORY
# setopt HASH_CMDS
setopt MENUCOMPLETE
setopt ALL_EXPORT

### Set/unset  shell options
############################
setopt   notify globdots correct pushdtohome cdablevars autolist
setopt   correctall autocd recexact longlistjobs
setopt   autoresume histignoredups pushdsilent
setopt   autopushd pushdminus extendedglob rcquotes mailwarning
unsetopt bgnice autoparamslash

### Autoload zsh modules when they are referenced
#################################################
autoload -U history-search-end
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
#zmodload -ap zsh/mapfile mapfile
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

### Set variables
#################
PATH="/usr/local/bin:/usr/local/sbin/:$PATH"
HISTFILE=$HOME/.zhistory
HISTSIZE=10000
SAVEHIST=10000
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';

### Load colors
###############
autoload colors zsh/terminfo
if [[ "$terminfo[colors]" -ge 8 ]]; then
   colors
fi
for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
   eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
   eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
   (( count = $count + 1 ))
done

### Set Colors to use in in the script
#############
# Normal Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

NC="\e[m"               # Color Reset

### Set prompt
##############
PR_NO_COLOR="%{$terminfo[sgr0]%}"
PS1="[%(!.${PR_RED}%n.$PR_LIGHT_YELLOW%n)%(!.${PR_LIGHT_YELLOW}@.$PR_RED@)$PR_NO_COLOR%(!.${PR_LIGHT_RED}%U%m%u.${PR_LIGHT_GREEN}%U%m%u)$PR_NO_COLOR:%(!.${PR_RED}%2c.${PR_BLUE}%2c)$PR_NO_COLOR]%(?..[${PR_LIGHT_RED}%?$PR_NO_COLOR])%(!.${PR_LIGHT_RED}#.${PR_LIGHT_GREEN}$) "
RPS1="$PR_LIGHT_YELLOW(%D{%m-%d %H:%M})$PR_NO_COLOR"
unsetopt ALL_EXPORT

### set common functions
#############

function my_ip() # Get IP adress.
{
   curl ifconfig.co
}

# Find a file with a pattern in name:
function ff()
{
    find . -type f -iname '*'"$*"'*' -ls ;
}



function sysinfo()   # Get current host related info.
{
    echo -e "\n${BRed}System Informations:$NC " ; uname -a
    echo -e "\n${BRed}Online User:$NC " ; w -hs |
             cut -d " " -f1 | sort | uniq
    echo -e "\n${BRed}Date :$NC " ; date
    echo -e "\n${BRed}Server stats :$NC " ; uptime
    echo -e "\n${BRed}Memory stats :$NC " ; free
    echo -e "\n${BRed}Public IP Address :$NC " ; my_ip
    echo -e "\n${BRed}Open connections :$NC "; netstat -pan --inet;
    echo -e "\n${BRed}CPU info :$NC "; cat /proc/cpuinfo ;
    echo -e "\n"
}

function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
 else
    if [ -f $1 ] ; then
        # NAME=${1%.*}
        # mkdir $NAME && cd $NAME
        case $1 in
          *.tar.bz2)   tar xvjf ../$1    ;;
          *.tar.gz)    tar xvzf ../$1    ;;
          *.tar.xz)    tar xvJf ../$1    ;;
          *.lzma)      unlzma ../$1      ;;
          *.bz2)       bunzip2 ../$1     ;;
          *.rar)       unrar x -ad ../$1 ;;
          *.gz)        gunzip ../$1      ;;
          *.tar)       tar xvf ../$1     ;;
          *.tbz2)      tar xvjf ../$1    ;;
          *.tgz)       tar xvzf ../$1    ;;
          *.zip)       unzip ../$1       ;;
          *.Z)         uncompress ../$1  ;;
          *.7z)        7z x ../$1        ;;
          *.xz)        unxz ../$1        ;;
          *.exe)       cabextract ../$1  ;;
          *)           echo "extract: '$1' - unknown archive method" ;;
        esac
    else
        echo "$1 - file does not exist"
    fi
fi
}


# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Create a ZIP archive of a file or folder.
function makezip() { zip -r "${1%%/}.zip" "$1" ; }


function my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ; }

mcd () {
    mkdir -p $1
    cd $1
}

### Set alias
#############
alias cls="clear"
alias ..="cd .."
alias cd..="cd .."
#alias ll="ls -lisa --color=auto"
alias ll="ls -h -lisa --color=auto"
alias home="cd ~"
alias df="df -ahiT --total"
alias mkdir="mkdir -pv"
alias mkfile="touch"
alias rm="rm -rfi"
alias userlist="cut -d: -f1 /etc/passwd"
alias ls="ls -CF --color=auto"
alias lsl="ls -lhFA | less"
alias free="free -mt"
alias du="du -ach | sort -h"
alias ps="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias wget="wget -c"
alias historyall="history 1 | less"
alias myip="curl http://ipecho.net/plain; echo"
alias logs="find /var/log -type f -exec file {} \; | grep 'text' | cut -d' ' -f1 | sed -e's/:$//g' | grep -v '[0-9]$' | xargs tail -f"
alias folders='find . -maxdepth 1 -type d -print0 | xargs -0 du -sk | sort -rn'
alias grep='grep --color=auto'


## my alias

# alias aweconfig="sudo nano /etc/xdg/awesome/rc.lua"
# alias termconfig="sudo nano /etc/xdg/termite/config"
# alias awetheme="sudo nano /usr/share/awesome/themes/default/theme.lua"
alias lightdmconfig="sudo vim /etc/lightdm/lightdm.conf"
alias restartlightdm="sudo systemctl restart lightdm"
alias enablelightdm="sudo systemctl enable lightdm"
alias startlightdm="sudo systemctl start lightdm"

#alias scan="clamscan --max-filesize=2000M --max-scansize=2000M --detect-pua --include-pua=CAT -r -i -l clamav.log --exclude-dir=Music --exclude-dir=Videos --exclude-dir=Pictures --exclude-dir=Documents --exclude-dir=Wallpapers --exclude-dir=Fall --exclude-dir=Phone --exclude-dir=share  --exclude-dir=ISO --exclude-dir=Makevideos"

alias scan="clamscan --max-filesize=2000M --max-scansize=2000M --detect-pua --include-pua=CAT -r -i -l clamav.log --exclude-dir=Music --exclude-dir=Videos --exclude-dir=Pictures --exclude-dir=Documents --exclude-dir=Wallpapers --exclude-dir=Fall --exclude-dir=Phone --exclude-dir=ISO"


alias scanall="clamscan --max-filesize=2000M --max-scansize=2000M --detect-pua --include-pua=CAT -r -i -l clamav.log"


#alias zhistory="nano .zhistory"
#alias addfastkeys="nano .zshrc"
alias restartshell="source ~/.zshrc"
alias changemirrors="sudo vim /etc/pacman.d/mirrorlist"
alias catmirrors="cat /etc/pacman.d/mirrorlist"
alias update="sudo pacman -Syu"
#alias aurcheck="paru -Qua"
#alias aurupdate="paru -Sua"
alias count="find . -maxdepth 1 -type f | wc -l"
# alias awedirtheme="cd /home/../usr/share/awesome/themes/default/"
# alias copyaweconfig="sudo cp rc.lua /etc/xdg/awesome"
# alias copyawetheme="sudo cp theme.lua /usr/share/awesome/themes/default"
# alias copytermite="sudo cp config /etc/xdg/termite"
alias qtileconfig="vim .config/qtile/config.py"
alias aweconfig="vim .config/awesome/rc.lua"
alias awetheme="vim .config/awesome/theme.lua"
# alias configterm="nano .config/termite/config"
alias mount="sudo mount /dev/sdb1 /mnt"
alias umount="sudo umount /dev/sdb1"
alias poweroff="udisksctl power-off -b /dev/sdb"
alias usblive="cat Instructions/usblive.txt"
alias mkgrub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias getmirrors="sudo curl -o /etc/pacman.d/mirrorlist https://archlinux.org/mirrorlist/all/"
alias refresh="sudo pacman -Syy"
alias refreshupdate="sudo pacman -Syyu"
alias lessmirrors="cat /etc/pacman.d/mirrorlist | less"
alias getstable="sudo pacman -S linux"
alias rmstable="sudo pacman -Rcns linux"
alias getlts="sudo pacman -S linux-lts"
alias rmlts="sudo pacman -Rcns linux-lts"
#alias configalacritty="vim .config/alacritty/alacritty.yml"
alias qtileauto="vim .config/qtile/autostart.sh"
alias weather="curl wttr.in"
alias pack="pacman -Qq | less"
#alias xconfig="vim .xmonad/xmonad.hs"
#alias mobarconfig="vim .config/xmobar/xmobar.config"
alias converttovm=""echo" sudo qemu-img convert -f vdi -O qcow2 Debian.vdi /var/lib/libvirt/images/Debian.qcow2"
alias converttovb=""echo" sudo qemu-img convert -O vdi win10.qcow2 win10.vdi"
alias virtvms="cd /var/lib/libvirt/images"
#alias herbconfig="vim .config/herbstluftwm/autostart"
alias install="sudo pacman -S"
alias remove="sudo pacman -Rcns"
alias vmshare="sudo mount -t 9p -o trans=virtio /sharepoint share"
alias ramcapacity="sudo dmidecode -t 16"
alias raminstalled="sudo dmidecode -t 17 | less"
alias checkdep="pacman -Qdtq"
alias removedep="sudo pacman -R $(pacman -Qdtq)"
alias bios="sudo dmidecode -s bios-version"
alias serial="sudo dmidecode -t 1"
alias failedlogin="sudo lastb"
alias lastreboot="last reboot -10"
#alias last="last | less"
alias cloneme="git clone https://gitlab.com/artibus1/mydotfiles.git"
alias s="startx"
alias vimx="vim .xinitrc"
alias unlock="sudo cryptsetup open /dev/sdb drive"
alias mountlock="sudo mount /dev/mapper/drive /mnt"
alias umountlock="sudo umount /mnt"
alias lock="sudo cryptsetup close drive"
alias lastupdate="grep -iE "$(tail -n1 /var/log/pacman.log | grep -iEo "([0-9]{4}-[0-9]{2}-[0-9]{2})").+upgraded" /var/log/pacman.log"
alias allupdates="grep -i upgraded /var/log/pacman.log"
alias l="ls -l"
#alias convert="ffmpeg -i *.mp4 *.mp3" 
alias sshopen="sudo systemctl start sshd.service"
alias sshclose="sudo systemctl stop sshd.service"
alias sshstatus="sudo systemctl status sshd.service"
alias firewallopen="./.openfirewall.sh"
alias firewallclose="./.closefirewall.sh"
alias firewallstatus="sudo ufw status verbose"
alias failtoban="sudo systemctl status fail2ban"
alias sha="sha256sum -c --ignore-missing sha256sums.txt"
alias blake="b2sum -c --ignore-missing b2sums.txt"
alias i3config="vim ~/.config/i3/config"
alias i3statusconfig="vim ~/.config/i3status/config"
alias aweauto="vim ~/.config/awesome/autostart.sh"



#bindkey -v

### Bind keys
#############
autoload -U compinit
compinit
bindkey "^?" backward-delete-char
bindkey '^[OH' beginning-of-line
bindkey '^[OF' end-of-line
bindkey '^[[5~' up-line-or-history
bindkey '^[[6~' down-line-or-history
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey "^r" history-incremental-search-backward
bindkey ' ' magic-space    # also do history expansion on space
bindkey '^I' complete-word # complete on tab, leave expansion to _expand
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=1 _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'

# Completion Styles

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate

# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'

# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# command for process lists, the local web server details and host completion
# on processes completion complete all user processes
zstyle ':completion:*:processes' command 'ps -au$USER'

## add colors to processes for kill completion
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

#zstyle ':completion:*:processes' command 'ps -o pid,s,nice,stime,args'
#zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'
#
#NEW completion:
# 1. All /etc/hosts hostnames are in autocomplete
# 2. If you have a comment in /etc/hosts like #%foobar.domain,
#    then foobar.domain will show up in autocomplete!
zstyle ':completion:*' hosts $(awk '/^[^#]/ {print $2 $3" "$4" "$5}' /etc/hosts | grep -v ip6- && grep "^#%" /etc/hosts | awk -F% '{print $2}')
# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
    '*?.old' '*?.pro'
# the same for old style completion
#fignore=(.o .c~ .old .pro)

# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm apache bin daemon games gdm halt ident junkbust lp mail mailnull \
        named news nfsnobody nobody nscd ntp operator pcap postgres radvd \
        rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs avahi-autoipd\
        avahi backup messagebus beagleindex debian-tor dhcp dnsmasq fetchmail\
        firebird gnats haldaemon hplip irc klog list man cupsys postfix\
        proxy syslog www-data mldonkey sys snort
# SSH Completion
zstyle ':completion:*:scp:*' tag-order \
   files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order \
   files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
   users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order \
   hosts-domain hosts-host users hosts-ipaddr
zstyle '*' single-ignored show

### Source plugins
##################
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
