#!/usr/bin/env bash

mkdir ~/.config &

cp .xinitrc .Xresources .xprofile .zshrc .zhistory .imwheelrc ~/ &

cp city.jpeg DarkCyan.png archlinux-simplyblack.png ~/ &

cp -R awesome qtile  ~/.config &

sudo cp lightdm.conf lightdm-gtk-greeter.conf /etc/lightdm/ &

sudo cp vimrc vconsole.conf /etc/ &

sudo cp .zshrc .zhistory /root &

sudo systemctl enable bluetooth.service &

sudo systemctl enable lightdm &

sudo ufw default deny &
sudo ufw enable &
sudo systemctl enable ufw &
sudo systemctl start ufw &
sudo ufw status









  
   
