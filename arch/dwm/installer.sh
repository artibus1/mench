#!/usr/bin/env bash



sudo pacman -S \
	dmenu \
	nitrogen \
	xorg-xsetroot \
	slock \
	wget \
	rofi
		

mkdir ~/Builds/dwm/ &

mkdir ~/Patches/ &

cp -R rofi ~/.config &

git clone https://git.suckless.org/dwm ~/Builds/dwm/ &

cp -R .dwm ~/ &

sudo cp config.h ~/Builds/dwm/ &

sudo cp dwm.desktop /usr/share/xsessions/ &

wget https://dwm.suckless.org/patches/autostart/dwm-autostart-20210120-cb3f58a.diff

mv dwm-autostart-20210120-cb3f58a.diff ~/Patches/ &

cp runpatch.sh ~/Builds/dwm/ 













