#!/usr/bin/sh 


start() {
  [ -z "$(pidof -x $1)" ] && ${2:-$1} &
}

xrdb .Xresources &
xset s off -dpms &
#nitrogen --restore
start lxsession &
start numlockx &
start volumeicon &
start imwheel 
xrandr -s 1360x768 
#xfce4-power-manager --power manager 


