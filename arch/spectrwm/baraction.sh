#!/bin/sh
# Example Bar Action Script for Linux.
# Requires: acpi, iostat.
# Tested on: Debian 10, Fedora 31.
#





## VOLUME 
vol() {     vol=`amixer get Master | awk -F'[][]' 'END{ print $2,$4 }' | sed 's/on://g'` 
	    echo -e "Vol: $vol"
}





# Cache the output of acpi(8), no need to call that every second.
ACPI_DATA=""
I=0
while :; do
	IOSTAT_DATA=`/usr/bin/iostat -c | grep '[0-9]$'`
	if [ $I -eq 0 ]; then
		ACPI_DATA=`/usr/bin/acpi -a 2>/dev/null; /usr/bin/acpi -b 2>/dev/null`
	fi
	# print_date
#	print_mem
#	print_cpu $IOSTAT_DATA
#	print_cpuspeed
#	print_bat $ACPI_DATA
	echo "$(vol)"
	I=$(( ( ${I} + 1 ) % 11 ))
	sleep 1
done
