#!/usr/bin/env bash



sudo apt install \
	awesome \
	clamav \
	cmus \
	dmenu \
	i3 \
	nitrogen \
	gimp \
	gufw \
	htop \
	imwheel \
	kdenlive \
	lxappearance \
	neofetch \
	numlockx \
	obs-studio \
	papirus-icon-theme \
	pcmanfm \
	remmina \
	vim \
	volumeicon-alsa \
	xfce4-power-manager \
	xterm \
	zsh \
