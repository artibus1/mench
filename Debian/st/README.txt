
To use my installer to install st terminal, use one of the following:
----------------------------------------------------------------------------------------------

To install st terminal without having installed dwm window manager, run the following command:

./installNoDWM.sh

----------------------------------------------------------------------------------------------

To install st terminal after installing dwm window manager, run the following command:

./installAfterDWM.sh


----------------------------------------------------------------------------------------------



To manually install st terminal:


git clone https://git.suckless.org/st

cd st

sudo make clean install

https://st.suckless.org/patches/scrollback/st-scrollback-0.9.2.diff

patch --dry-run < ~/Downloads/st-scrollback-0.9.2.diff

patch -p1 <  ~/Downloads/st-scrollback-0.9.2.diff

after installing patch

copy and paste the font line to config.def.h

static char *font = "monospace:style=bold:size=15";


sudo cp config.def.h config.h

sudo make clean install




To remove it:

sudo make uninstall


