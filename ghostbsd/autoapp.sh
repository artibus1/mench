#!/usr/bin/env sh

sudo pkg install \
       awesome \
       dmenu \
       htop \
       i3 \
       i3status \
       i3lock \
       libreoffice \
       librewolf \
       lxappearance \
       nitrogen \
       pcmanfm \
       remmina \
       thunderbird \
       vim \
       volumeicon \
       xfce4-power-manager \
       zsh


  
   
