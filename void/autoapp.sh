#!/usr/bin/env bash



sudo xbps-install \
	awesome \
	dmenu \
	htop \
	i3 \
	i3status \
	libreoffice \
	lxappearance \
	neofetch \
	nitrogen \
	papirus-icon-theme \
	pcmanfm \
	pulseaudio \
	qtile \
	remmina \
	rhythmbox \
	ufw \
	vim \
	xorg \
	xrandr \
	xterm \
	zsh




