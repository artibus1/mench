#!/usr/bin/env sh

sudo pkg install \
       awesome \
       baobab \
       celluloid \
       cinnamon \
       cmus \
       dmenu \
       eog \
       fastfetch \
       firefox \
       gnome-calculator \
       gnome-system-monitor \
       htop \
       i3 \
       i3status \
       i3lock \
       libqalculate \
       lightdm \
       lightdm-gtk-greeter \
       libreoffice \
       librewolf \
       lxappearance \
       materia-gtk-theme \
       neofetch \
       nitrogen \
       papirus-icon-theme \
       pavucontrol \
       pcmanfm \
       remmina \
       rhythmbox \
       thunderbird \
       vim \
       volumeicon \
       xed \
       xfce4-power-manager \
       xorg \
       zsh


  
   
